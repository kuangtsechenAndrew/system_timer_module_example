/*
 * timer.c
 *
 *  Created on: 2021�~1��8��
 *      Author: andre
 */

#include "timer.h"
#include "MPC5744P.h"
#include "typedefs.h"

#define MAX_EVENT_NUMBER 10

static timer_even TimerEventTable[MAX_EVENT_NUMBER];
static uint8_t NumberOfRegisteredEven = 0;
static void my_dummy(void);

static uint32_t MilliSecond = 0;
static uint32_t Second = 0;

/*******************************************************************************
 Function Name : get_milli_second
 Engineer      : Andrew
 Date          : Jan-11-2016
 Parameters    : NONE
 Modifies      : NONE
 Returns       : NONE
 Notes         : Return time past since STM is enable  in milli-second format,
                     It will overflow in about 49 days.
 Issues        : NONE
 *******************************************************************************/
uint32_t get_milli_second(void)
{
    return MilliSecond;
}

/*******************************************************************************
 Function Name : get_second
 Engineer      : Andrew
 Date          : Jan-11-2016
 Parameters    : NONE
 Modifies      : NONE
 Returns       : NONE
 Notes         : return time past since STM is enable  in second format
 Issues        : NONE
 *******************************************************************************/
uint32_t get_second(void)
{
    return Second;
}

void my_dummy(void)
{
    /*Do nothing here,*/
}

/*******************************************************************************
 Function Name : get_available_timer_event
 Engineer      : Andrew
 Date          : Jan-11-2016
 Parameters    : NONE
 Modifies      : NONE
 Returns       : NONE
 Notes         : return number of available timer event
 Issues        : NONE
 *******************************************************************************/
uint8_t get_available_timer_event(void)
{
    return NumberOfRegisteredEven;
}

/*******************************************************************************
 Function Name : STM_init
 Engineer      : Andrew
 Date          : Jan-8-2016
 Parameters    : NONE
 Modifies      : NONE
 Returns       : NONE
 Notes         : STM initialisation
 Issues        : NONE
 *******************************************************************************/
void stm_init(void)
{
    /* Disable STM  */
    STM_0.CR.R = 0x0;

    /*Disable channel 0*/
    STM_0.CHANNEL[0].CCR.R = 0x00;

    /* Set up compare value */
    /* 1ms for an interrupt */
    STM_0.CHANNEL[0].CMP.R = 50000U;

    /*Enable channel 0*/
    STM_0.CHANNEL[0].CCR.R = 0x01;

    /* Enable STM and set counter Pre-scaler to  0x0 Divide system clock by 1 */
    STM_0.CR.R = 0x001;

    stm_even_table_init( );
}

/*******************************************************************************
 Function Name : stm_even_enable
 Engineer      : Andrew
 Date          : Jan-8-2016
 Parameters    : NONE
 Modifies      : NONE
 Returns       : NONE
 Notes         : Enable even
 Issues        : NONE
 *******************************************************************************/
void stm_even_enable(callback_void_t CallbackFunc, bool State)
{
    uint8_t index = 0U;

    for ( index = 0; index < NumberOfRegisteredEven ; index++ )
    {
        if (TimerEventTable[index].CallbackFunct == CallbackFunc)
        {
            TimerEventTable[index].EnableState = State;
        }
    }

}

/*******************************************************************************
 Function Name : stm_isr
 Engineer      : Andrew
 Date          : Jan-8-2016
 Parameters    : NONE
 Modifies      : NONE
 Returns       : NONE
 Notes         : ISR which cause by interrupt,
 Issues        : NONE
 *******************************************************************************/
void stm_isr(void)
{
    static uint32_t LocalCounter = 1U;
    uint8_t index = 0U;

    /*Clear interrupt flag*/
    STM_0.CHANNEL[0].CIR.R = 0x01;
    STM_0.CNT.R = 0;

    /* Execute  timer event if the period is achieved*/
    for ( index = 0; index < NumberOfRegisteredEven ; index++ )
    {
        if ((LocalCounter % TimerEventTable[index].Period) == 0U)
        {
            if (TimerEventTable[index].EnableState == TRUE)
            {
                TimerEventTable[index].CallbackFunct( );
            }
        }
    }

    if (LocalCounter == 0xFFFFFFFF)
    {
        LocalCounter = 0;
    }
    else
    {
        LocalCounter++;
    }

    if ((LocalCounter % 1000) == 0)
    {
        if (Second == 0xFFFFFFFF)
        {
            Second = 0;
        }
        else
        {
            Second++;
        }
    }

    MilliSecond = LocalCounter;
}

/*******************************************************************************
 Function Name : stm_even_table_init
 Engineer      : Andrew
 Date          : Jan-8-2016
 Parameters    : NONE
 Modifies      : NONE
 Returns       : NONE
 Notes         : Given table dummy ISR.
 Issues        : NONE
 *******************************************************************************/
void stm_even_table_init()
{
    uint8_t index = 0;

    for ( index = 0; index < MAX_EVENT_NUMBER ; index++ )
    {
        stm_even_register(my_dummy, 1000);
    }

    /*Clean up the counter*/
    NumberOfRegisteredEven = 0;

}

/*******************************************************************************
 Function Name : stm_even_register
 Engineer      : Andrew
 Date          : Jan-8-2021
 Parameters    : Callback function name and Period
 Modifies      : NONE
 Returns       : Successful or not to register a timer event
 Notes         : Register a callback function with an assigned period
 Issues        : NONE
 *******************************************************************************/
bool stm_even_register(callback_void_t FunctPtr, uint32_t Period)
{
    bool ResultFlag = FALSE;

    /*Check input parameter valid or not*/
    if ((&FunctPtr > 0) && (Period > 0))
    {
        /*Check available event slot first*/
        if (NumberOfRegisteredEven < MAX_EVENT_NUMBER)
        {
            TimerEventTable[NumberOfRegisteredEven].CallbackFunct = FunctPtr;
            TimerEventTable[NumberOfRegisteredEven].Period = Period;
            TimerEventTable[NumberOfRegisteredEven].EventArryIndex = NumberOfRegisteredEven;

            ResultFlag = TRUE;
            NumberOfRegisteredEven++;
        }
    }

    return ResultFlag;
}

